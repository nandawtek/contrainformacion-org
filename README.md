# Contrainformación

Contrainformación es un proyecto open source que busca aunar el esfuerzo de tantos compañeros por la información de lo que ocurre en nuestros barrios y ciudades.

Contrainformación no persigue ser una plataforma de lectura, si no facilitar el acceso a los artículos de contrainformación publicados en otros medios pudiendo filtrar estos por fechas, contenido y etiquetas, y acceder así a las noticias publicadas en varios medios y radios libres.

## Participa/Colabora

Puedes colaborar con Contrainformacion sugiriendo medios libres para añadir a nuestro listado enviando un email a `sugerencias@contrainformacion.org`

## License

GNU GENERAL PUBLIC LICENSE Version 3 
@NandawteK. [View License](LICENSE.md)
